package com.ws.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;

/**
 * The Class StrUtils.
 */
public class StrUtils {
	
	/** The Constant HEX. */
	private final static char[] HEX = {
		'0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
	};

    /** The Constant HEXES. */
    static final String HEXES = "0123456789ABCDEF";
	
	/**
	 * Str.
	 *
	 * @param o the s
	 * @return the string
	 */
	public static String str(Object o) {
		return str(o, "");
	}
	
	/**
	 * Str.
	 *
	 * @param o the o
	 * @param dflt the dflt
	 * @return the string
	 */
	public static String str(Object o, String dflt) {
		return o != null ? String.valueOf(o) : dflt;
	}
	
	/**
	 * Join.
	 *
	 * @param <T> the generic type
	 * @param col the col
	 * @param sep the sep
	 * @return the string
	 */
	public static <T> String join(Collection<T> col, String sep) {
		StringBuilder sb = new StringBuilder();
		Iterator<T> iter = col.iterator();
		while(iter.hasNext()) {
			if (sb.length() > 0) {
				sb.append(sep);
			}
			sb.append(iter.next());
		}
		return sb.toString();
	}
	
	/**
	 * Prints the stack trace.
	 *
	 * @param e the e
	 * @return the string
	 */
	public static String printStackTrace(Throwable e) {
		String s;
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			s = sw.toString();
		} catch (Throwable e2) {
			s = new StringBuilder().append("printStackTrace failed, error: ").append(e2.getMessage())
			.append(", original error: ").append(e.getMessage())
			.toString();
		} finally {
			IOUtils.closeQuietly(pw);
			IOUtils.closeQuietly(sw);
		}
		return s;
	}
	
	/**
	 * From long.
	 *
	 * @param value the value
	 * @return the string
	 */
	public static String fromLong(long value) {
		char[] hexs = new char[16];		
		for (int i = 0; i < 16; i++) {
			int c = (int)(value & 0xf);
			hexs[16 - i - 1] = HEX[c];
			value = value >> 4;
		}
		return new String(hexs);
	}

    /**
     * To hex.
     *
     * @param raw the raw
     * @param len the len
     * @param dflt the dflt
     * @return the string
     */
    public static String toHex(byte[] raw, int len, String dflt) {
        if (raw == null) {
            return dflt;
        }
        if (len > raw.length) {
        	len = raw.length;
        }
        StringBuilder hex = new StringBuilder(2 * len);
        for (int i = 0; i < len; i++) {
        	byte b = raw[i];
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }
    
    /**
     * To hex.
     *
     * @param raw the raw
     * @return the string
     */
    public static String toHex(byte[] raw, int len) {
    	return toHex(raw, len, "null");
    }
}
