package com.ws.messaging.rabbitmq;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;

import com.rabbitmq.client.Channel;

public class CAMessageListenerImpl implements ChannelAwareMessageListener {
	public void onMessage(Message message, Channel channel) throws Exception {
		System.out.println("onMessage, channel: " + channel + ", message: " + message);
	}
}
