package com.ws.messaging.rabbitmq;

/**
 * The Class MessagePojo.
 */
public class MessagePojo {
	
	/** The value. */
	private String value;
 
	/**
	 * Instantiates a new message pojo.
	 *
	 * @param value the value
	 */
	public MessagePojo(String value) {
		this.value = value;
	}
 
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
