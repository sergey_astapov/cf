package com.ws.messaging.rabbitmq;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.io.IOUtils;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.xml.XppReader;

public class RabbitmqTest extends TestCase {
	public RabbitmqTest(String method) {
		super(method);
	}
	
	public void testSimple() {
		ConnectionFactory connectionFactory = new CachingConnectionFactory();
		AmqpAdmin admin = new RabbitAdmin(connectionFactory);
		admin.declareQueue(new Queue("myqueue"));
		AmqpTemplate template = new RabbitTemplate(connectionFactory);
		template.convertAndSend("myqueue", "foo");
		String foo = (String)template.receiveAndConvert("myqueue");
		assertNotNull(foo);
	}
	
	public void testXstream() throws IOException, ClassNotFoundException {
		ConnectionFactory connectionFactory = new CachingConnectionFactory();
		AmqpAdmin admin = new RabbitAdmin(connectionFactory);
		admin.declareQueue(new Queue("myqueue"));
		AmqpTemplate template = new RabbitTemplate(connectionFactory);
        XStream xstream = new XStream();
		ByteArrayOutputStream baos = null;
		ObjectOutputStream out = null;
		MessagePojo pojo1 = new MessagePojo("pojo");
        try {
        	baos = new ByteArrayOutputStream();
            out = xstream.createObjectOutputStream(new OutputStreamWriter(baos));
            out.writeObject(pojo1);
        } finally {
        	IOUtils.closeQuietly(out);
        	IOUtils.closeQuietly(baos);
        }
        
        template.convertAndSend("myqueue",  new String(baos.toByteArray(), Charset.forName("UTF-8")));
		String foo = (String)template.receiveAndConvert("myqueue");
		assertNotNull(foo);
		
		HierarchicalStreamReader xreader = new XppReader(new StringReader(foo));
		ObjectInputStream in = null;
		try {
			in = xstream.createObjectInputStream(xreader);
	        MessagePojo pojo2 = (MessagePojo)in.readObject();
			assertNotNull(pojo2);
			assertEquals(pojo1.getValue(), pojo2.getValue());
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	public void testMessage() {
		ConnectionFactory connectionFactory = new CachingConnectionFactory();
		AmqpAdmin admin = new RabbitAdmin(connectionFactory);
		admin.declareQueue(new Queue("myqueue"));
		AmqpTemplate template = new RabbitTemplate(connectionFactory);
		MessageProperties properties = new MessageProperties();
		properties.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN);
		Message outMessage = new Message("foo".getBytes(Charset.forName("UTF-8")), properties);
		template.send("myqueue", outMessage);
		Message inMessage = template.receive("myqueue");
		assertNotNull(inMessage);
		assertEquals(new String(inMessage.getBody(), Charset.forName("UTF-8")),
				new String(inMessage.getBody(), Charset.forName("UTF-8")));
	}
	
	public void testXml() {
		ApplicationContext context = new GenericXmlApplicationContext("classpath:/rabbit-context.xml");
		AmqpTemplate template = context.getBean(AmqpTemplate.class);
		template.convertAndSend("myqueue", "foo");
		String foo = (String) template.receiveAndConvert("myqueue");
		assertNotNull(foo);
	}
	
	public void testConfiguration() {
		ApplicationContext context = new AnnotationConfigApplicationContext(RabbitConfiguration.class);
		AmqpTemplate template = context.getBean(AmqpTemplate.class);
		template.convertAndSend("myqueue", "foo");
		String foo = (String) template.receiveAndConvert("myqueue");
		assertNotNull(foo);
	}
	
	public void testAsyncXml() throws InterruptedException {
		ApplicationContext context = new GenericXmlApplicationContext("classpath:/rabbit-context-2.xml");
		AmqpTemplate template = context.getBean(AmqpTemplate.class);
		template.convertAndSend("myqueue", "foo");
		TimeUnit.MILLISECONDS.sleep(5000);
	}
	
	public void testMessageConverter() throws IOException, ClassNotFoundException {
		ApplicationContext context = new GenericXmlApplicationContext("classpath:/rabbit-context-3.xml");
		AmqpTemplate template = context.getBean(AmqpTemplate.class);
		MessagePojo pojo1 = new MessagePojo("pojo");
		template.convertAndSend("myqueue", pojo1);
		MessagePojo pojo2 = (MessagePojo) template.receiveAndConvert("myqueue");
		assertNotNull(pojo2);
		assertEquals(pojo1.getValue(), pojo2.getValue());
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite();
		//suite.addTest(new RabbitmqTest("testSimple"));
		//suite.addTest(new RabbitmqTest("testMessage"));
		//suite.addTest(new RabbitmqTest("testXml"));
		//suite.addTest(new RabbitmqTest("testConfiguration"));
		//suite.addTest(new RabbitmqTest("testXstream"));
		//suite.addTest(new RabbitmqTest("testAsyncXml"));
		suite.addTest(new RabbitmqTest("testMessageConverter"));
		return suite;
	}
	
	public void main(String[] args) {}
}
