package com.ws.messaging.rabbitmq;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.amqp.support.converter.MessageConverter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.xml.XppReader;

/**
 * The Class XStreamMessageConverterImpl.
 */
public class XStreamMessageConverterImpl implements MessageConverter {
	
	/* (non-Javadoc)
	 * @see org.springframework.amqp.support.converter.MessageConverter#toMessage(java.lang.Object, org.springframework.amqp.core.MessageProperties)
	 */
	public Message toMessage(Object object, MessageProperties messageProperties) throws MessageConversionException {
		ByteArrayOutputStream baos = null;
		ObjectOutputStream out = null;
        try {
        	baos = new ByteArrayOutputStream();
        	XStream xstream = new XStream();
    		out = xstream.createObjectOutputStream(new OutputStreamWriter(baos));
            out.writeObject(object);
            return new Message(baos.toByteArray(), messageProperties);
        } catch (IOException e) {
        	throw new MessageConversionException(e.getMessage());
        } finally {
        	IOUtils.closeQuietly(out);
        	IOUtils.closeQuietly(baos);
        }
	}

	/* (non-Javadoc)
	 * @see org.springframework.amqp.support.converter.MessageConverter#fromMessage(org.springframework.amqp.core.Message)
	 */
	public Object fromMessage(Message message) throws MessageConversionException {
		HierarchicalStreamReader xreader = new XppReader(new StringReader(new String(message.getBody(), Charset.forName("UTF-8"))));
		ObjectInputStream in = null;
		try {
        	XStream xstream = new XStream();
			in = xstream.createObjectInputStream(xreader);
	        return in.readObject();
		} catch (ClassNotFoundException e) {
        	throw new MessageConversionException(e.getMessage());			
		} catch (IOException e) {
        	throw new MessageConversionException(e.getMessage());
		} finally {
			IOUtils.closeQuietly(in);
		}
	}
}
